# GhSearch

Sample app to let users browse through the repositories hosted on [Github](https://github.com).

####Steps to run the app

- Clone the repository
- Install dependencies  `npm install`
- Start the server `npm start`
- Fire up your browser at [http://localhost:4200](http://localhost:4200)

#####Environment Dependencies
 - Node - v6.9.1
 - NPM - 3.10.8