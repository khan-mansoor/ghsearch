import { GhSearchPage } from './app.po';

describe('gh-search App', () => {
  let page: GhSearchPage;

  beforeEach(() => {
    page = new GhSearchPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
